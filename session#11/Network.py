# This module holds all relevant methods for migrating OpenStack Compute Services
__author__ = "Jan Frömberg"
__copyright__ = "Copyright 2022, Cloud and Heat Technologies GmbH"
__credits__ = ["Jan Frömberg"]
__license__ = "Apache License 2.0"
__version__ = "1.0.0"
__email__ = "jan.froemberg@cloudandheat.com"

import openstack
import OpenStackConnection as OSC
 
conn1 = OSC.Connection('gaiax-cloud-a')
conn2 = OSC.Connection('gaiax-cloud-b')

""" 
compute instances migration
"""
def migrateNetworkTopology():
    print("Kindly implement the net topology migration for an OpenStack")

""" 
compute images migration
"""
def migrateNetworks():
    print("Kindly implement the network migration for an OpenStack")

""" 
compute keypais migration
"""
def migrateRouters():
    print("Kindly implement the routers migration for an OpenStack")

""" 
compute keypais migration
"""
def migrateSecurityGroups():
    print("Kindly implement the sec groups migration for an OpenStack")

""" 
compute keypais migration
"""
def migrateFloatingIPs():
    print("Kindly implement the floating IPs migration for an OpenStack")


"""
Start all migration processes all together
"""
def startAll() :
    migrateNetworks()
    migrateFloatingIPs()
    migrateNetworkTopology()
    migrateRouters()
    migrateSecurityGroups()