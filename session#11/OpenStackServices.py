## OpenStack Supported Services: Images (glance), Block Storage (cinder), Compute (Nova), 
# Network (neutron), Identity (keystone), KeyManagement (Barbican), LoadBalancer (octavia)
#
# This module uses the OpenStack SDK: https://pypi.org/project/openstacksdk/
# Please install via virtual env (python3 -m venv .venv) 
# and use the requirements.txt (pip install -r requirements.txt).
__author__ = "Jan Frömberg"
__copyright__ = "Copyright 2022, Cloud and Heat Technologies GmbH"
__credits__ = ["Jan Frömberg"]
__license__ = "Apache License 2.0"
__version__ = "1.0.0"
__email__ = "jan.froemberg@cloudandheat.com"


import openstack
import abc

import Network as n
import Compute as c
import Identity as i
import Volumes as v
import ObjectStore as o
import ContainerInfra as ci
import LoadBalancer as l
import KeyManager as k

"""
An Migration-Interface to define needed methods for each service migration 
"""
class MigrationInterface(abc.ABC):
    @abc.abstractmethod
    def migrate( self ):
        pass


""" 
This is the class for the OpenStack Compute Service
"""
class ComputeMigration(MigrationInterface) :

    def __init__(self, name):
        self.name = name

    def migrate(self) -> None:
        c.startAll()


""" 
This is the class for the OpenStack Network Service
"""
class NetworkMigration(MigrationInterface) :

    def __init__(self, name):
        self.name = name

    def migrate(self) -> None:
        n.startAll()


""" 
This is the class for the OpenStack Identity Service
"""
class IdentityMigration(MigrationInterface) :
    
    def __init__(self, name):
        self.name = name

    def migrate(self) -> None:
        i.startAll()


""" 
This is the class for the OpenStack KeyManagement Service
"""
class VolumeMigration(MigrationInterface) :

    def __init__(self, name):
        self.name = name

    def migrate(self) -> None:
        v.startAll()


""" 
This is the class for the OpenStack KeyManagement Service
"""
class ContinerInfraMigration(MigrationInterface) :

    def __init__(self, name):
        self.name = name

    def migrate(self) -> None:
        ci.startAll()


""" 
This is the class for the OpenStack BlockStorage Service
"""
class BlockStorageMigration(MigrationInterface) :

    def __init__(self, name):
        self.name = name

    def migrate(self) -> None:
        o.startAll()


""" 
This is the class for the OpenStack Loadbalancer Service
"""
class LoadBalancerMigration(MigrationInterface) :

    def __init__(self, name):
        self.name = name

    def migrate(self) -> None:
        l.startAll()


""" 
This is the class for the OpenStack KeyManager Service
"""
class KeyManagerMigration(MigrationInterface) :

    def __init__(self, name):
        self.name = name

    def migrate(self) -> None:
        k.startAll()