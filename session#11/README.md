# Hackathon #4 Session #11

## Prerequisites

### 1. Install python requirements (using python virtual envirnoment)

Create python virtual environment

```
python3 -m venv /path/to/new/virtual/environment
```

Load virtual environment

```
. /path/to/new/virtual/environment/bin/acivate
```


Install project requirements

```
pip install -r requirements.txt
```


### 2. Install kubectl

https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/#install-kubectl-binary-with-curl-on-linux


### 3. Cloud&Heat Login Credentials

Load `openrc/cnh-openrc.sh`

`source openrc/cnh-openrc.sh`

Password: Will be announced during the session.

### 4. SCS Login Credentials


### How to start  

Implementations has to be done in each service module file .py. For example Compute.py / Identity.py / aso.
Please use the ```OpenStack.Connection``` class to establish a configured connection to Openstack.

Each connection can be configured in ```clouds.yaml``` file

In the end please start the migration with:

```python

$ python3 cli_start_migration.py

collected sd files: provider.json
contend of a given sd-file: Nameless Edge Cloud GmbH support@namelessedgecloud.de

---------------------------Start Migration---------------------------------

Kindly implement the obj store container migration
Kindly implement the network migration for an OpenStack
Kindly implement the floating IPs migration for an OpenStack
Kindly implement the net topology migration for an OpenStack
Kindly implement the routers migration for an OpenStack
Kindly implement the sec groups migration for an OpenStack
Kindly implement the instances migration for an OpenStack
Kindly implement the images migration for an OpenStack
Kindly implement the key pais migration for an OpenStack
Kindly implement the identity.user migration
Kindly implement the identity.prj migration
Kindly implement the cluster template migration for an OpenStack
Kindly implement the cluster migration for an OpenStack
Kindly implement the volumes migration for an OpenStack
Kindly implement the cg snapshots migration for an OpenStack
Kindly implement the cg migration for an OpenStack
Kindly implement the snapshots migration for an OpenStack
Kindly implement the loadbalancer migration for an OpenStack
Kindly implement the keymanager migration for an OpenStack

```


